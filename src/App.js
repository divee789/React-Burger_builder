import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom"
import Layout from "./hoc/Layout/Layout"
import Burger from "./containers/burger/Burger"
import Checkout from "./containers/Checkout/Checkout"
import Orders from "./containers/Orders/Orders"
class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Switch>
            <Route path="/checkout" component={Checkout} />
            <Route path="/orders" exact component={Orders} />
            <Route path="/" exact component={Burger} />
          </Switch>
        </Layout>
      </div>
    );
  }
}
//ROUTING PRAGMATICALLY
//this.props.history.push('route)

export default App;
