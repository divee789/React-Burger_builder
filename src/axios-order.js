import axios from "axios"

const instance = axios.create({
    baseURL: "https://lendgap-bce9c.firebaseio.com/"
})

export default instance