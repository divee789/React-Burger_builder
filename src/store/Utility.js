export const updatedObject = (oldProp,updatedProperties) =>{
     return{
         ...oldProp,
         ...updatedProperties
     }
}