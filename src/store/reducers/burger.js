import * as actioTypes from '../actions/actionTypes'
import { updatedObject } from '../Utility'

const initialState = {
  ingredients: null,
  totalPrice: 4,
  error: false
}
const INGREDIENT_PRICES = {
  salad: 1,
  meat: 2,
  cheese: 1.5,
  bacon: 2.5
}

const reducer = (state = initialState, action) => {

  switch (action.type) {

    case actioTypes.ADD_INGREDIENT:

      const updatedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] + 1 }

      const updatedIngredients = updatedObject(state.ingredients, updatedIngredient)

      const updatedState = {
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName]
      }

      return updatedObject(state, updatedState);

    case actioTypes.REMOVE_INGREDIENT:

      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [action.ingredientName]: state.ingredients[action.ingredientName] - 1
        },
        totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName]
      }
      
    case actioTypes.FAILED_INGREDIENTS:
      return {
        ...state,
        error: true
      }
    case actioTypes.SET_INGREDIENTS:
      return {
        ...state,
        ingredients: action.ingredients,
        error: false,
        totalPrice: 4
      }
    default:
      return state
  }

}

export default reducer 