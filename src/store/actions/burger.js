import * as actionTypes from './actionTypes'
import axios from "../../axios-order"


export const addIngredient = (name) => {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingredientName: name
    }
}
export const removeIngredient = (name) => {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingredientName: name
    }
}
export const failedIngredient = () => {
    return {
        type: actionTypes.FAILED_INGREDIENTS
    }
}

export const setIngredients = (ing) => {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients: ing
    }
}

//Allowed by redux-thunk
export const initIngredient = () => {
    return dispatch => {
        axios.get("https://lendgap-bce9c.firebaseio.com/ingredients.json").then(response => {
            dispatch(setIngredients(response.data))
        }).catch(error =>
            console.log(error)
        )
    }
}