import React, { Component } from 'react'
import Aux from "../Auxilliary/Auxillary"
import classes from "./Layout.css"
import Toolbar from "../../components/Navigation/Toolbar/Toolbar"
import SideDrawer from "../../components/Navigation/SideDrawer/SideDrawer"


class Layout extends Component {
    state = {
        showSideDrawer:false
    }

    SideDrawerClosedHandler = () => {
        this.setState({showSideDrawer:false})
    }
    SideDrawerOpenHandler = () => {
        this.setState({showSideDrawer:true})
    }
    render() {
        return (
            <Aux>
                <Toolbar clicked={this.SideDrawerOpenHandler}/>
                 
                <SideDrawer closed={this.SideDrawerClosedHandler}
                open={this.state.showSideDrawer}/>


                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
}
export default Layout