import React from "react";
import classes from "./Controls.css"
import btn from "./button.css"
import Control from "./Control/Control"

const controls = [
    { label: "Salad", type: 'salad' },
    { label: "Cheese", type: 'cheese' },
    { label: "Meat", type: 'meat' },
    { label: "Bacon", type: 'bacon' }
]
const Controls = (props) => (
    <div className={classes.BuildControls}>
        <p>Current Price:<strong>{props.price.toFixed(2)} Dollars</strong></p>
        {controls.map(ctrl=>(
             <Control key={ctrl.label} label={ctrl.label} added={()=> props.ingredientAdded(ctrl.type)}
             removed={()=>props.ingredientRemoved(ctrl.type)} disabled={props.disabled[ctrl.type]}/>
        ))}  
        <button 
        className={btn.OrderButton}
        disabled={!props.purchaseable}
        onClick={props.ordered}>ORDER NOW</button>
      </div>
)

export default Controls