import React from "react";
import classes from './Burger.css'
import Ingredient from "./Ingredients/Ingredients"

const Burger = (props) => {
    //Object.keys()m  brigs out the keys of object as array   

    
    let transformedIngredients = Object.keys(props.ingredients).map(igKey => {
        return [...Array(props.ingredients[igKey])].map((_, i) => {
            return <Ingredient key={igKey + i} type={igKey} />
        })
    }).reduce((arr, el) => {
        return arr.concat(el)
    }, []);




    console.log(transformedIngredients)
   
    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding Ingredients</p>
    }

    return (
        <div className={classes.Burger}>
            <Ingredient type="bread-top" />
            {transformedIngredients}
            <Ingredient type="bread-bottom" />
        </div>
    )
}

export default Burger