import React,{Component} from 'react'
import Aux from '../../../hoc/Auxilliary/Auxillary'
import Button  from "../../UI/Button/Button"

class Order extends Component  {

 render(){
    const ingredientSummary = Object.keys(this.props.ingredients).map(igKey => {
        return <li key={igKey}><span>{igKey}:</span>{this.props.ingredients[igKey]}</li>
    })
     return(
            <Aux>
                <h3>Your Order</h3>
                <p>A Delicious Burger with the following Ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed(2) }</strong></p>
              <p>Continue to Checkout?</p>
                <Button btnType="Danger" clicked={this.props.cancel}>Cancel</Button>
                <Button btnType="Success" clicked={this.props.continue}>Continue</Button>
            </Aux>
        
     )
  }
}

export default Order