import React, { Component } from 'react';
import { connect } from 'react-redux'
import Aux from "../../hoc/Auxilliary/Auxillary"
import Burgers from "../../components/Burger/Burger"
import BuildControl from "../../components/Burger/Controls/Controls"
import Modal from '../../components/UI/Modal/Modal'
import Order from "../../components/Burger/Order/Order"
import Spinner from "../../components/UI/Spinner/Spinner"
import WithErrorHandler from "../../hoc/WithErrorHandler/WithErrorHandler"
import axios from "../../axios-order"
import * as burgerActions from '../../store/actions/index'


class Burger extends Component {
    // constructor(props){
    //     super(props)
    //     this.state={...}
    // }
    state = {
        purchaseable: false,
        purchasing: false
    }

    componentDidMount() {
        console.log(this.props)
        this.props.onInitIngredients()
    }
    updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients).map(igKey => {
            return ingredients[igKey]
        }).reduce((sum, el) => {
            return sum + el
        }, 0)
        return sum > 0
    }

    purchaseHandler = () => {
        this.setState({ purchasing: true })
    }
    closeModalHandler = () => {
        this.setState({ purchasing: false })
    }
    purchaseContinueHandler = async () => {
        this.props.onInitPurchase()
        this.props.history.push({
            pathname: "/checkout"
        })
    }

    render() {

        const disabledInfo = {
            ...this.props.ings
        }
        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let OrderSummary = null

        let burger = this.props.error ? <p>Ingredients can't be loaded</p> : <Spinner />

        if (this.props.ings) {
            burger = (
                <Aux>
                    <Burgers ingredients={this.props.ings} />
                    <BuildControl
                        ingredientAdded={this.props.onIngredientAdded}
                        ingredientRemoved={this.props.onIngredientRemoved}
                        disabled={disabledInfo}
                        price={this.props.price}
                        purchaseable={this.updatePurchaseState(this.props.ings)}
                        ordered={this.purchaseHandler} />
                </Aux>
            )

            OrderSummary = <Order ingredients={this.props.ings}
                cancel={this.closeModalHandler}
                continue={this.purchaseContinueHandler}
                price={this.props.price} />
        }

        return (
            <Aux>
                <Modal show={this.state.purchasing}
                    modalClosed={this.closeModalHandler}>
                    {OrderSummary}
                </Modal>
                {burger}
            </Aux>
        )
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burger.ingredients,
        price: state.burger.totalPrice,
        error: state.burger.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded: (igName) => dispatch(burgerActions.addIngredient(igName)),
        onIngredientRemoved: (igName) => dispatch(burgerActions.removeIngredient(igName)),
        onInitIngredients: () => dispatch(burgerActions.initIngredient()),
        onInitPurchase: () => dispatch(burgerActions.purchaseInit())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WithErrorHandler(Burger, axios))